import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    newsData: Array<any>;
    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private router: Router) {
  }
  title = 'GoldenOak';
}
