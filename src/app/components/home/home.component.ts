import { Component, OnInit } from '@angular/core';
import { Subscriber } from '../contact/models/contact.model';
import { ContactService } from '../contact/services/contact.service';
import { AppConfig } from '../_config/app.config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ContactService]
})
export class HomeComponent implements OnInit {
  newsData: Array<any>;
  url = 'assets/css/multiCarousel.js';
  subscriber: Subscriber = new Subscriber();
  submitted = false;
  error = '';
  WebUrl = new AppConfig().webUrl;
  candleLink = '/products?category=' + '2DBEBD6F-B18B-492D-BDBD-1D61F5901BB9';
  constructor(private contact: ContactService) {
  this.newsData = [
    {
      title: 'https://www.amazon.in/s?k=pan+aromas',
      src: '../assets/amazon.png'
    },
    {
      title: 'https://www.nykaa.com/search/result/?q=Pan+Aromas',
      src: '../assets/nykaa.png'
    },
    {
      title: 'https://www.flipkart.com/search?q=Pan%20Aromas',
      src: '../assets/flipkart.png'
    }
  ];
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = this.url;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
}

  ngOnInit() {
    this.loadScript();
  }

  // addSubscriber() {
  //   console.log(this.subscriber);
  //   this.error = '';
  //   this.contact.Subscribe(this.subscriber).subscribe(response => {
  //    console.log(response);
  // }, error => {
  //     console.log(error);
  // });
  // }

}
