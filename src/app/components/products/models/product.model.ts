export class Product {
    id: number;
    productId: string;
    productName: string;
    itemCode: string;
    package: number;
    specification1: string;
    specification2: string;
    specification3: string;
    fullDescription: string;
    useCase: string;
    hostIp: string;
    createdBy: string;
    createdDate: Date;
    updatedBy: string;
    updatedDate: Date;
    isActive: boolean;
    isDeleted: boolean;
}
