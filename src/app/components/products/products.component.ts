import { Component, OnInit } from '@angular/core';
import { MatDialog, PageEvent } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import { ProductsService } from './services/product.service';
import { AppConfig } from '../_config/app.config';
import { PageSize } from '../_config/PageSize.config';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService]
})
export class ProductsComponent implements OnInit {
  newsData: Array<any>;
  displayedColumns = ['id', 'name', 'fragrance'];
  dataSource: Array<any>;
  fragrances: Array<any>;
  options = {
    timeOut: 3000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true
  };
  selectedIndex = [];
  fragranceImage = [];
  fullProductData: Array<any>;
  ImageUrl = new AppConfig().ImageUrl;
  PageSize = new PageSize().PageSize;
  currentPage: any;
  totalRecords: number;
  productId: any;
  loading: boolean;
  constructor(public dialog: MatDialog, private productService: ProductsService, private route: ActivatedRoute) {
    this.newsData = [
      {
        title: 'https://www.amazon.in/s?k=pan+aromas',
        src: '../assets/amazon.png'
      },
      {
        title: 'https://www.nykaa.com/search/result/?q=Pan+Aromas',
        src: '../assets/nykaa.png'
      },
      {
        title: 'https://www.flipkart.com/search?q=Pan%20Aromas',
        src: '../assets/flipkart.png'
      }
    ];
  }


  openDialog(productId) {
    // console.log(Id);
    // tslint:disable-next-line:prefer-const
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '80vw',
      maxHeight: '80vh',
      data: productId
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/css/multiCarousel.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  selectFragrance(product, fragrance) {
    this.fragranceImage[product] = this.fragrances[product][fragrance].fileName;
    this.selectedIndex[product] = fragrance;
    console.log(this.selectedIndex);
  }


  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'auto'
    });
  }

  public handlePage(e: any): PageEvent {
    this.gotoTop();
    this.currentPage = e.pageIndex;
    if (this.productId.params.id != null) {
      this.GetClientPageListListing(this.currentPage + 1, this.productId.params.id);
    } else if (this.productId.params.category != null) {
      this.GetClientPageListListing(this.currentPage + 1, this.productId.params.category);
    } else {
      this.GetClientPageListListing(this.currentPage + 1);
    }
    // this.GetClientPageListListing(this.currentPage + 1);
    return;
  }

  GetClientPageListListing(pageno: number, id?: string) {
    this.loading = true;
    if (id == null) {
      this.productService.GetAllProductPageList(pageno, this.PageSize).subscribe(
        data => {
          this.loading = false;
          this.fullProductData = data;
          this.dataSource = data.products;
          this.totalRecords = data.totalRecords;
          this.fragrances = data.productFragrances;
          this.selectedIndex = [];
          this.fragranceImage = [];
          for (let i = 0; i < data.productFragrances.length; i++) {
            this.fragranceImage.push(this.fragrances[i][0].fileName);
            this.selectedIndex.push(0);
          }
          console.log(this.selectedIndex);
          console.log(this.fragrances);
        }
      );
    } else {
      this.productService.GetAllProductPageListByCategory(pageno, this.PageSize, id).subscribe(
        data => {
          this.loading = false;
          this.fullProductData = data;
          this.dataSource = data.products;
          this.totalRecords = data.totalRecords;
          this.fragrances = data.productFragrances;
          this.selectedIndex = [];
          this.fragranceImage = [];
          for (let i = 0; i < data.productFragrances.length; i++) {
            this.fragranceImage.push(this.fragrances[i][0].fileName);
            this.selectedIndex.push(0);
          }
          console.log(this.selectedIndex);
          console.log(this.fragrances);
        }
      );
    }
  }

  ngOnInit() {
    this.loadScript();
    this.route.queryParamMap.subscribe(params => {
      this.productId = params;
      console.log(this.productId.params.category);
    });
    // this.dataSource = [
    //   { id: 1, title: 'AMAZON'} , { id: 1, title: 'NYKAA' }, { id: 1, title: 'FLIPKART' },
    //   { id: 1, title: 'SNAP DEAL'}, { id: 1, title: 'GO DADDY' }, { id: 1, title: 'OLX'}
    // ];
    if (this.productId.params.id != null) {
      this.GetClientPageListListing(1, this.productId.params.id);
    } else if (this.productId.params.category != null) {
      this.GetClientPageListListing(1, this.productId.params.category);
    } else {
      this.GetClientPageListListing(1);
    }
  }

}
