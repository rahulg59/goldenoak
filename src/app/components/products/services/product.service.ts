import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AppConfig } from '../../_config/app.config';
import { Product } from '../models/product.model';

@Injectable()
export class ProductsService {

    public apiUrl: string;

    constructor(private http: Http, private route: Router) {
        this.apiUrl = new AppConfig().apiUrl;
    }

    GetProductbyId(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetFragrancesbyId(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/fragrance/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProduct() {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products', options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProductPageList(pageno, pagesize) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/' + pageno + '/' + pagesize, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProductPageListById(pageno, pagesize, Id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/id/' + Id + '/' + pageno + '/' + pagesize, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProductPageListByCategory(pageno, pagesize, Id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/category/' + Id + '/' + pageno + '/' + pagesize, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    public AddProduct(ProductModel: Product) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Products', ProductModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

}

