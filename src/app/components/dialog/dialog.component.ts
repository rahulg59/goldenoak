import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductsService } from '../products/services/product.service';
import { Product } from '../products/models/product.model';
import { AppConfig } from '../_config/app.config';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  providers: [ProductsService]
})


export class DialogComponent {
    product: Product = new Product();
    fragrances: Array<any>;
    ImageUrl = new AppConfig().ImageUrl;
    WebUrl = new AppConfig().webUrl;
    fragranceImage: any;
    selectedIndex: any;
    constructor(
        public dialogRef: MatDialogRef<DialogComponent>,
        private productService: ProductsService,
        @Inject(MAT_DIALOG_DATA) public data: any) {
          this.selectedIndex = 0 ;
          this.productService.GetProductbyId(data).subscribe(
            result => {
              this.product = result;
              console.log(this.product);
            }
          );
          this.productService.GetFragrancesbyId(data).subscribe(
            result => {
              this.fragrances = result;
              this.fragranceImage = this.fragrances[0].fileName;
              console.log(this.fragrances);
            }
          );
          this.WebUrl = encodeURI(this.WebUrl + '/products?id=' + data);
          console.log(this.WebUrl);
        }
        selectImage(index) {
          this.selectedIndex = index;
          this.fragranceImage = this.fragrances[index].fileName;
        }
}
