import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AppConfig } from '../../_config/app.config';
import { Contact, Subscriber, MailChimpResponse } from '../models/contact.model';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class ContactService {

    public apiUrl: string;
    data: any;
    public addSubscriberUrl = 'https://us3.api.mailchimp.com/3.0/lists/d0ab397ff3';
    mailChimpEndpoint =
    'https://goldenoak.us3.list-manage.com/subscribe/post?u=f7620a7c0cc396dc9cdb69779&amp;id=d0ab397ff3&subscribe=Subscribe&';
    submitted: boolean;
    error: string;

    constructor(private http: Http, private route: Router, private httpClient: HttpClient) {
        this.apiUrl = new AppConfig().apiUrl;
    }

    public contactMail(ContactModel: Contact) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Contact', ContactModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

    public PartnerMail(ContactModel: Contact) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Partner', ContactModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

    // public Subscribe(subscriber: Subscriber) {
    //     const params = new HttpParams()
    //         .set('EMAIL', subscriber.email_address)
    //         .set('b_f7620a7c0cc396dc9cdb69779_d0ab397ff3', ''); // hidden input name
    //     const mailChimpUrl = this.mailChimpEndpoint + params.toString();

    //     // 'c' refers to the jsonp callback param key. This is specific to Mailchimp
    //     return this.httpClient.jsonp<MailChimpResponse>(mailChimpUrl, 'c');
    // }

    // public Subscribe(subscriber: Subscriber) {
    //     // this.data = JSON.stringify(subscriber);
    //     console.log(this.data);
    //     const headers = new Headers({
    //     'Content-Type': 'application/json',
    //     'Authorization' : 'Basic Basic Z29sZGVub2FraW5kaWE6Mjc3MzMxMDBhMWVmNmE1ZTE1NGVlMWU4NTg2NzI1NDUtdXMz'
    //     });
    //     const options = new RequestOptions('{ headers: headers }');
    //     return this.http.post(this.addSubscriberUrl, this.data, options)
    //         .map((res: Response) => res.json())
    //         .catch(response => {
    //             if (response.status === 401) {
    //                 this.route.navigate(['Login']);
    //             }
    //             return response;
    //         });
    // }
}



