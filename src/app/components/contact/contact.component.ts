import { Component, OnInit } from '@angular/core';
import { Contact } from './models/contact.model';
import { ContactService } from './services/contact.service';
import { ToastyConfig, ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  providers: [ContactService, ToastyConfig, ToastyService ]
})
export class ContactComponent implements OnInit {
  newsData: Array<any>;
  lat = 19.0787903;
  lng = 72.9100531;
  Contact: Contact = new Contact();
  loading = false;
  constructor(private contactService: ContactService, private toastyConfig: ToastyConfig, private toastyService: ToastyService) {
    this.newsData = [
    {
      title: 'https://www.amazon.in/s?k=pan+aromas',
      src: '../assets/amazon.png'
    },
    {
      title: 'https://www.nykaa.com/search/result/?q=Pan+Aromas',
      src: '../assets/nykaa.png'
    },
    {
      title: 'https://www.flipkart.com/search?q=Pan%20Aromas',
      src: '../assets/flipkart.png'
    }
  ];
    this.toastyConfig.position = 'top-center';
    this.toastyConfig.theme = 'bootstrap';
  }

  ngOnInit() {
    this.loadScript();
  }

  public loadScript() {
    const node = document.createElement('script');
    node.src = 'assets/css/multiCarousel.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
}
  sendMail() {
    this.loading = true;
    this.contactService.contactMail(this.Contact).subscribe(
      data => {
        this.loading = false;
        this.toastyService.success('Mail Send, We will Contact you within 48 hours.');
      }, error => {
        this.loading = false;
        this.toastyService.error('Failed.');
      }
    );
  }

}
